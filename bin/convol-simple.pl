#!/usr/bin/perl -w
# convol-simple.pl --- Simple convolution running
# Author: Pavel Kačer <pavel@draculus.de>
# Created: 24 Jan 2013
# Version: 0.01

use warnings;
use strict;

use PicLib;

sub usage {
    my $exit_status = shift // 0;
    print ("Usage: $0 FILE OUTPUT CONVOLUTION-FILE\n");
    exit ($exit_status);
}

usage (1) unless @ARGV > 2;

my $source_file = $ARGV[0] // usage (1);
my $output_file = $ARGV[1] // usage (1);
my $convolution_file = $ARGV[2] // usage (1);

print ("Reading input file '${source_file}'.\n");
my $in_img = Image::Imlib2->load ($source_file);
print ("Reading convolution kernel from '${convolution_file}'.\n");
print ("Processing image.\n");
my @convol = PicLib->load_file_kernel ($convolution_file);
my $out_img = PicLib->convolution ($in_img, \@convol);
print ("Writing to file '${output_file}'.\n");
$out_img->save ($output_file);
print ("Done.\n");

__END__

=head1 NAME

edge-simple.pl - Describe the usage of script briefly

=head1 SYNOPSIS

edge-simple.pl [options] args

      -opt --long      Option description

=head1 DESCRIPTION

Stub documentation for edge-simple.pl, 

=head1 AUTHOR

Pavel Kačer, E<lt>pavel@draculus.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013 by Pavel Kačer

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

None reported... yet.

=cut
