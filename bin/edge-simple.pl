#!/usr/bin/perl -w
# edge-simple.pl --- Simple edge detection algorithm
# Author: Pavel Kačer <pavel@draculus.de>
# Created: 24 Jan 2013
# Version: 0.01

use warnings;
use strict;

use PicLib;

my $source_file = $ARGV[0] // die ("Usage: $0 FILE OUTPUT [TRESHOLD]\n");
my $output_file = $ARGV[1] // die ("Usage: $0 FILE OUTPUT [TRESHOLD]\n");
my $treshold = $ARGV[2] // 70;

print ("Reading input file '$source_file'.\n");
my $in_img = Image::Imlib2->load ($source_file);
print ("Processing image.\n");
my $out_img = PicLib->edge_simple ($in_img, $treshold);
print ("Writing to file '$output_file'.\n");
$out_img->save ($output_file);
print ("Done.\n");

__END__

=head1 NAME

edge-simple.pl - Describe the usage of script briefly

=head1 SYNOPSIS

edge-simple.pl [options] args

      -opt --long      Option description

=head1 DESCRIPTION

Stub documentation for edge-simple.pl, 

=head1 AUTHOR

Pavel Kačer, E<lt>pavel@draculus.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013 by Pavel Kačer

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

None reported... yet.

=cut
